ARG WORKDIR="/app"
ARG APPNAME="my-app"

FROM python:3.11-slim-bookworm

ARG WORKDIR
ARG APPNAME

ENV PYTHONBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

RUN apt-get update && apt-get install --assume-yes --no-install-recommends \
    gcc-11=11.3.0-12 build-essential=12.9

# создается системный пользователь (--system) \
# и группа для него с таким же id как и у самого пользователя (--group) \
# так же задается домашний каталог для нового созданного пользователя (--home ${WORKDIR}) \
# ${APPNAME} - имя нового пользователя
RUN adduser --system --group --home ${WORKDIR} ${APPNAME}

# выбирается определенный пользователь от имени которого\
# будут производиться дальнейшие команды внутри контейнера
USER ${APPNAME}

# переход в директорию из которой будет вестись дальнейшая работа в контейнере
WORKDIR /home/${APPNAME}${WORKDIR}

COPY ./requirements.txt /home/${APPNAME}${WORKDIR}

# "-m" запускает библиотеку(модуль) по имени, а не по названию файла, в данном случае - это запуск pip-а
RUN python -m pip install pip~=23.0 --upgrade --no-cache-dir && python -m pip install --requirement "requirements.txt" --no-cache-dir

COPY ./app .

# "-m" запускает библиотеку(модуль) по имени, а не по названию файла,\
# в данном случае - это запуск main модуля, а не файла main.py (хотя в данном случае main.py и есть main модуль)
CMD [ "python", "-m", "main" ]