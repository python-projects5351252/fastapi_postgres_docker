import bcrypt
from typing import Union


def hash_password(password: str) -> bytes:
    salt = bcrypt.gensalt()
    hashed = bcrypt.hashpw(str.encode(password), salt)
    return hashed


def verify_password(password: str, hashed_password: Union[str, bytes]) -> bool:
    if isinstance(hashed_password, str):
        if bcrypt.checkpw(str.encode(password), str.encode(hashed_password)):
            return True
        return False
    else:
        if bcrypt.checkpw(str.encode(password), hashed_password):
            return True
        return False
