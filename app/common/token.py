import time
from common.hash import hash_password, verify_password


def set_token():
    try:
        unixtime_now = int(time.time())
        # print(f"Bearer {unixtime_now}")
        token = hash_password(f"Bearer {unixtime_now}")  # -> bytes
    except Exception as e:
        return {'status': 'fail', 'details': e}
    return {'status': 'success', 'details': f'{token.decode("utf-8")}'}  # не забываем преобразовать bytes в str
