import uvicorn
from fastapi import FastAPI
# from dotenv import dotenv_values
from core.server import create_app
from modules.user import user_controller
from modules.group import group_controller


def setup_routes(server: FastAPI):
    server.include_router(user_controller.router, tags=["users"])
    server.include_router(group_controller.router, tags=["groups"])


app = create_app()
setup_routes(app)


def main() -> None:
    """Invoke the entrypoint function of the script."""
    uvicorn.run("main:app", host="0.0.0.0", reload=True)


if __name__ == "__main__":
    main()
