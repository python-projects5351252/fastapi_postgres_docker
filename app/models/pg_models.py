from peewee import Model, CharField, DateField, BooleanField, ManyToManyField, ForeignKeyField
from auth.database import pg_db


class BaseModel(Model):
    class Meta:
        database = pg_db  # This model uses the "test" database.


class Users(BaseModel):
    name = CharField()
    email = CharField(unique=True)

    class Meta:
        # table_name = 'users'
        legacy_table_names = False


class User(BaseModel):
    # модель таблицы Пользователей
    name = CharField()
    login = CharField(unique=True, null=False, max_length=20)
    email = CharField(unique=True)
    password = CharField(null=False)
    is_active = BooleanField(default=True)
    is_admin = BooleanField(default=False)
    created_at = DateField()  # auto_now_add=True

    class Meta:
        # table_name = 'users'
        legacy_table_names = False  # то как peewee будет интерпретировать имя класса в имя таблицы \
        # (User -> user; PeopleProfile -> people_profile; Users -> users)


class Groups(BaseModel):
    # модель таблицы Групп (many-to-many)
    name = CharField(null=False, unique=True)
    users = ManyToManyField(Users, backref='groups')

    class Meta:
        legacy_table_names = False


class Token(BaseModel):
    # модель таблицы Токенов
    user_id = ForeignKeyField(User, backref='tokens')
    token = CharField(null=False, unique=True)
    created_at = DateField()


def create_tables():
    with pg_db:
        pg_db.create_tables([User, Groups, Token], safe=True)
