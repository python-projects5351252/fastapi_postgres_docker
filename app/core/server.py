from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from contextlib import asynccontextmanager
from models.pg_models import create_tables


@asynccontextmanager
async def lifespan(app: FastAPI):
    """
    Создаем таблицы в БД перед запуском приложения
    docs: https://fastapi.tiangolo.com/advanced/events/
    """
    create_tables()
    yield


def create_app() -> FastAPI:
    app = FastAPI(
        debug=True,
        title="FastAPI API",
        root_path="/",
        lifespan=lifespan,
        docs_url="/docs"
    )

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    return app
