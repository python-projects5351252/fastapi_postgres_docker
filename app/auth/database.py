# from sqlalchemy import create_engine
# import psycopg2
from peewee import *
from dotenv import dotenv_values

cfg = dotenv_values('.env')

# Connect to a Postgres database.
pg_db = PostgresqlDatabase(
    cfg['POSTGRES_DB'],
    user=cfg['POSTGRES_USER'],
    password=cfg['POSTGRES_PASSWORD'],
    host='dockerized_app_postgres',
    port=cfg['POSTGRES_PORT']
)

# POSTGRESQL_URL = "postgresql://che:qwerty@dockerized_app_postgres/test"
# engine = create_engine(POSTGRESQL_URL)
# def get_cur():
#     try:
#         conn = psycopg2.connect(
#             dbname=cfg['POSTGRES_DB'],
#             user=cfg['POSTGRES_USER'],
#             password=cfg['POSTGRES_PASSWORD'],
#             host='dockerized_app_postgres',
#             port=cfg['POSTGRES_PORT'])
#     except Exception as e:
#         print(e)
#         return {}
#     else:
#         cur = conn.cursor()
#         return cur, conn
