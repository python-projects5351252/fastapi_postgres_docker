from typing import List

from pydantic import BaseModel, Field


class UserResponse(BaseModel):
    name: str = Field(examples=["Bob"])
    email: str = Field(examples=["bob@mail.ru"])


class ManyUsersResponse(BaseModel):
    users: List[UserResponse] = Field()
