from pydantic import BaseModel, Field, ConfigDict


class GroupDTO(BaseModel):
    # модель таблицы Групп (many-to-many)
    name: str
    model_config = ConfigDict(from_attributes=True)

    # users: ManyToManyField(Users, backref='groups')
    # class Config:
    #     orm_mode = True
