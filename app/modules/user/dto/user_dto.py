import datetime

from pydantic import BaseModel, Field


class UserDto(BaseModel):
    name: str
    login: str = Field(max_length=20)
    email: str
    password: str
    is_active: bool = Field(default=True)
    is_admin: bool = Field(default=False)

    # class Config:
    #     orm_mode = True


class UserResponseDto(BaseModel):
    id: int
    name: str
    login: str
    email: str
    _password: str
    is_active: bool
    is_admin: bool
    created_at: datetime.date

    # class Config:
    #     orm_mode = True


class UserLoginDTO(BaseModel):
    login: str
    password: str


class UserLoginResponseDto(BaseModel):
    # id: int
    # name: str
    # login: str
    # email: str
    password: str
    # is_active: bool
    # is_admin: bool
    # created_at: datetime.date
