import time
from fastapi import APIRouter, HTTPException
from common.hash import hash_password, verify_password
from common.token import set_token
from .dto.user_dto import UserDto, UserResponseDto, UserLoginDTO
from models.pg_models import User, Token

router = APIRouter(prefix="/users")


@router.get("/{user_id}", response_model=UserResponseDto)
async def get_user(user_id: int) -> UserResponseDto:
    """
    Get user information
    """
    try:
        user = User.get(User.id == user_id)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=404, detail=f"User with id ({user_id}) does not exist.")
    # print(user.password)
    # Пароль не будет показываться в итоговых данных т.к. в модели(UserResponseDto) он начинается с "_"
    return user


@router.post("/")
async def create_user(user_model: UserDto) -> int:
    """
    Create a new USER
    """
    new_model = user_model.model_dump()
    new_model["password"] = hash_password(new_model.get("password"))
    new_model["created_at"] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

    try:
        res = User.insert(**new_model).execute()
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    return res


@router.post("/login")
async def get_token(login_data: UserLoginDTO):
    """
    Login user (get Token)
    """
    try:
        db_user_data = User.get(User.login == login_data.login)  # достаем объект пользователя из БД
    except Exception as e:
        print(e)
        raise HTTPException(status_code=404, detail=f"User with login ({login_data.login}) does not exist.")

    # Данные пользователя успешно найдены в БД - можно взять ТОЛЬКО пароль для дальнейшего сравнения
    passwd = db_user_data.password

    # сравниваем введенный пользователем пароль (str) и пароль из БД (str):
    correct = verify_password(login_data.password, passwd)

    if not correct:
        raise HTTPException(status_code=422, detail=f"Wrong Password!")

    token = set_token()
    if token.get('status') != 'success':
        raise HTTPException(status_code=500, detail=token.get('details'))

    # TODO: тут нужно записать токен в БД

    return {'detail': token.get('details')}
