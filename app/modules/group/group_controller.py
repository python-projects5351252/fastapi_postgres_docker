from fastapi import APIRouter, HTTPException
from models.pg_models import Groups

router = APIRouter(prefix="/groups")


@router.post("/")
async def create_group(name: str):
    try:
        res = Groups.insert(name=name)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    return res[0]  # [i for i in res][0]
